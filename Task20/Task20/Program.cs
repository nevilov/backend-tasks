﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task20 {

    public class StudentGrades {
        public string Student { get; set; }
        public int number { get; set; }
        public int gradeHistory { get; set; }
        public int gradeGeography { get; set; }
        public int gradeMath { get; set; }

        public override string ToString() {
            return $"Студент {Student}";
        }
    }

    class Program {
        static Random rnd = new Random();
        static void Main(string[] args) {
            
            String[] names = { "Андрей", "Антон", "Денис", "Данил", "Анна", "Диана", "Евгений" };
            int indexName;

            var University = new List<StudentGrades>();

            for(int i=1; i <= 100; i++) {
                indexName = rnd.Next(names.Length);

                var student = new StudentGrades() {
                    Student = $"{names[indexName]} {i}",
                    gradeGeography = rnd.Next(1, 6),
                    gradeHistory = rnd.Next(1, 6),
                    gradeMath = rnd.Next(1, 6),
                    number = i
                };
                University.Add(student);
            }

            //The student who have 5 in history
            var result1 = from student in University
                         where student.gradeHistory == 2
                         select student;

            var sameResult1 = University.Where(student => student.gradeHistory == 2);

            foreach(var item in result1) {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            foreach (var item in sameResult1) {
                Console.WriteLine(item);
            }


            Console.WriteLine();

            //The student who have 2 in geography
            var result2 = University
                    .Where(student => student.gradeGeography == 2)
                    .OrderBy(s => s.Student)
                    .ToList();

            foreach (var item in result2) {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}

