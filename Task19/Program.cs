﻿using System;

namespace Task19 {
    class Program {
        static void Main(string[] args) {
            const int k = 30;
            int min = 100000;
            int temp;

            Random rnd = new Random();

            int[] a = new int[k];
            for(int i =0;i < k; i++) {
                a[i] = rnd.Next(-50, 50);
            }

            /*NonModif Array*/
            for (int i = 0; i < k; i++) {
                Console.Write(a[i] + "\t");
            }

            /*Sort*/
            //for (int i = 0; i < k; i++) {
            //    for(int j=0;j<k-1;j++) {
            //        if (a[j + 1] > a[j]) {
            //            temp = a[j + 1];
            //            a[j + 1] = a[j];
            //            a[j] = temp;
            //        }
            //    }
            //}

            Array.Sort(a);

            for (int i = 0; i < k; i++) {
                Console.Write(a[i] + "\t");
            }


            for (int i =0; i < k; i++) {
                if (a[i] < min)
                    min = a[i];
            }

            Console.WriteLine();
            Console.WriteLine($"Минимальное значение = {min}");
            Console.WriteLine($"Медианное значение = {a[15]}");
            Console.ReadKey();
        }
    }
}
