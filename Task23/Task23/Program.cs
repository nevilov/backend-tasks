﻿using System;

namespace Task23 {
    interface IMoveable {
        void Move();
    }

    //--------------//

    public abstract class Vehicle {

        public string Flight { get; set; }
        public Vehicle(string flight) {
            Flight = flight;
        }

        public abstract void Display();

    }

    public class Plane : Vehicle, IMoveable {
        public string Trip;
        public Plane(string flight, string trip) : base(flight) {
            Trip = trip;
        }
        public override void Display() {
            Console.WriteLine($"Полет {Flight}, курс {Trip}" );
        }

        public void Move() {
            throw new NotImplementedException();
        }
    }

    public class DeathStar : Vehicle {
        public string Stars { get; set; }
        public DeathStar (string flight, string stars):base(flight) {
            Stars = stars;
        }
        public override void Display() {
            Console.WriteLine($"Полет {Flight}, Звезда {Stars}");
        }
    }


    //--------------//

    public abstract class Animal {
        public string World { get; set; }
        public abstract void Display();

        public Animal(string world) {
            World = world; // Care
        }

    }
    public abstract class Mammal : Animal {
        public string KingDom { get; set; }
        public Mammal(string world, string kingDom) : base(world) {
            KingDom = kingDom;
        }
        public override void Display() {
            Console.WriteLine($"Животное {World}, Семейство {KingDom}");
        }
    }
    public class Horse : Mammal {
        public string Name { get; set; }
        public Horse(string world, string kingDom, string name) : base(world, kingDom) {
            Name = name;
        }
        public override void Display() {
            Console.WriteLine($"Животное {World}, Семейство {KingDom}, Имя {Name}");
        }
    }

    class Program {
        public static void Main(String[] argc) {
            Horse h = new Horse("Лошадь", "Златогривые", "Гаврюша");
            h.Display();

            Plane p = new Plane("Первый", "Северный");
            p.Display();
            DeathStar d = new DeathStar("Звездный", "Солнце");
            d.Display();


        }
    }
}